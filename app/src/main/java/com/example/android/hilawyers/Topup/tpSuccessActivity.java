package com.example.android.hilawyers.Topup;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.android.hilawyers.MainActivity;
import com.example.android.hilawyers.R;

public class tpSuccessActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tp_success);

        ActionBar bar = getSupportActionBar();
        bar.setTitle("Top Up");
    }

    public void goToHomeTopup(View view) {
        startActivity(new Intent(tpSuccessActivity.this, MainActivity.class));
        finish();
    }
}
