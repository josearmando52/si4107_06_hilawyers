package com.example.android.hilawyers.PendampinganKepolisian;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.android.hilawyers.LegalOpinion.loInformasiActivity;
import com.example.android.hilawyers.LegalOpinion.loKasusActivity;
import com.example.android.hilawyers.R;
import com.example.android.hilawyers.Utility.SharedPrefs;

public class pkKasusActivity extends AppCompatActivity {

    RadioGroup rbTipeKasus, rbJenisKasus1, rbJenisKasus2;

    boolean isCheckedJenis = false;
    int checkIdTipe = -1;
    int checkIdJenis = -1;
    SharedPrefs sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pk_kasus);
        ActionBar bar = getSupportActionBar();
        bar.setTitle("PEndampingan di Kepolisiann");

        sp = new SharedPrefs(this);

        rbTipeKasus = findViewById(R.id.rbTipeKasus);
        rbJenisKasus1 = findViewById(R.id.rbJenisKasus1);
        rbJenisKasus2 = findViewById(R.id.rbJenisKasus2);

        rbTipeKasus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                checkIdTipe = checkedId;
            }
        });

        rbJenisKasus1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId != -1 && !isCheckedJenis) {
                    isCheckedJenis = true;
                    rbJenisKasus2.clearCheck();
                    checkIdJenis = checkedId;
                }
                isCheckedJenis = false;
            }
        });

        rbJenisKasus2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId != -1 && !isCheckedJenis) {
                    isCheckedJenis = true;
                    rbJenisKasus1.clearCheck();
                    checkIdJenis = checkedId;
                }
                isCheckedJenis = false;
            }
        });
    }

    public void goToInformasiPK(View view) {
        if (checkIdTipe != -1 && checkIdJenis != -1) {
            RadioButton rbJenis = findViewById(checkIdJenis);

            sp.setValue(sp.KASUS, rbJenis.getText().toString());
            startActivity(new Intent(pkKasusActivity.this, pkInformasiActivity.class));
        } else {
            Toast.makeText(this, "Harap Pilih Jenis dan Tipe Kasus", Toast.LENGTH_SHORT).show();
        }
    }
}
