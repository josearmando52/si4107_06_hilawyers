package com.example.android.hilawyers.Topup;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.android.hilawyers.R;

public class tpNominalActivity extends AppCompatActivity {

    RadioGroup rgNominal1, rgNominal2;
    boolean isChecked = false;
    int checkId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tp_nominal);
        ActionBar bar = getSupportActionBar();
        bar.setTitle("Top Up");

        rgNominal1 = findViewById(R.id.rgNominal1);
        rgNominal2 = findViewById(R.id.rgNominal2);

        if (savedInstanceState != null) {
            checkId = savedInstanceState.getInt("Nominal");

            rgNominal1.check(checkId);
            rgNominal2.check(checkId);
        }

        rgNominal1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId != -1 && !isChecked) {
                    isChecked = true;
                    rgNominal2.clearCheck();
                    checkId = checkedId;
                }
                isChecked = false;
            }
        });

        rgNominal2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId != -1 && !isChecked) {
                    isChecked = true;
                    rgNominal1.clearCheck();
                    checkId = checkedId;
                }
                isChecked = false;
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("Nominal", checkId);
    }

    //Fitur Untuk memilih Nominal yang akan di tambah ke dalam Saldo topup

    public void goToReauth(View view) {
        if (checkId != 0 ) {
            Intent i = new Intent(tpNominalActivity.this,tpReauthActivity.class);
            switch (checkId) {
                case R.id.rbNominal1:
                    i.putExtra("nominal",1000000);
                    break;
                case R.id.rbNominal2:
                    i.putExtra("nominal",3000000);
                    break;
                case R.id.rbNominal3:
                    i.putExtra("nominal",5000000);
                    break;
                case R.id.rbNominal4:
                    i.putExtra("nominal",10000000);
                    break;
            }

            startActivity(i);

        } else {
            Toast.makeText(tpNominalActivity.this,"Pilih Salah Satu Nominal", Toast.LENGTH_SHORT).show();
            //jika tombol ditekan tanpa memilih nominal maka akan menampilkan Toast
        }
    }
}
