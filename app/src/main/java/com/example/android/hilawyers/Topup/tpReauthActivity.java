package com.example.android.hilawyers.Topup;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.hilawyers.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class tpReauthActivity extends AppCompatActivity {

    FirebaseUser fUser;
    EditText etPasswordTopup, etPasswordConfirmTopup;
    int balance = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tp_reauth);
        ActionBar bar = getSupportActionBar();
        bar.setTitle("Top Up");

        Intent i = getIntent();
        balance = i.getIntExtra("nominal",0);

        fUser = FirebaseAuth.getInstance().getCurrentUser();
        etPasswordTopup = findViewById(R.id.etPasswordTopup);
        etPasswordConfirmTopup = findViewById(R.id.etPasswordConfirmTopup);
    }

    public void topupBalance(View view) {
        if (TextUtils.equals(etPasswordTopup.getText(),etPasswordConfirmTopup.getText())) {
            final String email = fUser.getEmail();
            AuthCredential credential = EmailAuthProvider.getCredential(email,etPasswordConfirmTopup.getText().toString());
            fUser.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Intent in = new Intent(tpReauthActivity.this, tpLoadingActivity.class);
                        in.putExtra("nominal",balance);
                        startActivity(in);
                    } else {
                        etPasswordTopup.setError("Password Salah");
                        etPasswordConfirmTopup.setError("Password Salah");
                    }
                }
            });
        } else {
            etPasswordConfirmTopup.setError("Password Tidak Sama");
        }
    }
}
