package com.example.android.hilawyers.ui.home;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import com.example.android.hilawyers.Model.User;
import com.example.android.hilawyers.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

public class HomeViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public LiveData<String> getText() {
        if (mText == null) {
            mText = new MutableLiveData<String>();
            loadBalance();
        }
        return mText;
    }

    private void loadBalance() {
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.child("user").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).getValue(User.class);
                mText.setValue(String.valueOf(user.getBalance()));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                mText.setValue("-");
            }
        };
    }
}