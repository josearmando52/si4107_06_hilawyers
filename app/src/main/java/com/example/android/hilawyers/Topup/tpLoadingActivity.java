package com.example.android.hilawyers.Topup;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.android.hilawyers.LoginActivity;
import com.example.android.hilawyers.MainActivity;
import com.example.android.hilawyers.Model.User;
import com.example.android.hilawyers.R;
import com.example.android.hilawyers.SplashScreen;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class tpLoadingActivity extends AppCompatActivity {

    DatabaseReference db;
    FirebaseUser fUser;
    int nominal = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tp_loading);

        ActionBar bar = getSupportActionBar();
        bar.setTitle("Top Up");

        fUser = FirebaseAuth.getInstance().getCurrentUser();
        db = FirebaseDatabase.getInstance().getReference().child("user").child(fUser.getUid());

        Intent i = getIntent();
        nominal = i.getIntExtra("nominal",0);

        Thread timerThread = new Thread() {
            public void run() {
                try {
                    sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                   db.addListenerForSingleValueEvent(new ValueEventListener() {
                       @Override
                       public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                           long userBalance = (long) dataSnapshot.child("balance").getValue();
                            //Menambahkan Saldo yang tadi sudah dipilih
                           db.child("balance").setValue(userBalance + nominal);

                            // berpindah ke activity Top Up Telah berhasil dan selesai
                           startActivity(new Intent(tpLoadingActivity.this, tpSuccessActivity.class));
                           finish();
                       }

                       @Override
                       public void onCancelled(@NonNull DatabaseError databaseError) {

                       }
                   });
                }
            }
        };
        timerThread.start();

    }
}
