package com.example.android.hilawyers.Utility;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Map;

public class SP {

    public SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;

    public SP(Context context) {
        this.context = context;
        pref = context.getSharedPreferences("HiLawyers",Context.MODE_PRIVATE);
        editor = pref.edit();
    }

    public void setLoggedIn(boolean logged, Map<String,String> detail) {
        editor.putBoolean("logged",true);
        for (Map.Entry<String,String> entry : detail.entrySet()) {
            editor.putString(entry.getKey(),entry.getValue());
        }
        editor.apply();
    }

    public boolean checkLog() {
        return pref.getBoolean("logged",false);
    }

    public void logout() {
        editor.clear();
        editor.commit();
    }
}
