package com.example.android.hilawyers;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.hilawyers.Model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterActivity extends AppCompatActivity {

    EditText etEmailRegister, etPasswordRegister, etPhoneRegister;
    private FirebaseAuth mAuth;
    private DatabaseReference db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        etEmailRegister = findViewById(R.id.etEmailRegister);
        etPasswordRegister = findViewById(R.id.etPasswordRegister);
        etPhoneRegister = findViewById(R.id.etPhoneRegister);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseDatabase.getInstance().getReference().child("user");
    }


    public void register(View view) {
        if (TextUtils.isEmpty(etEmailRegister.getText())) {
            etEmailRegister.setError("Email Required");
        } else if (TextUtils.isEmpty(etPasswordRegister.getText())) {
            etPasswordRegister.setError("Password Required");
        } else if (etPasswordRegister.getText().length() < 8) {
            etPasswordRegister.setError("Minimum of 8 characters");
        } else if (TextUtils.isEmpty(etPhoneRegister.getText())) {
            etPhoneRegister.setError("Mobile Number Required");
        }

        mAuth.createUserWithEmailAndPassword(etEmailRegister.getText().toString(), etPasswordRegister.getText().toString())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            writeNewUser(user.getUid(),user.getEmail());
                        } else {
                            Toast.makeText(RegisterActivity.this,"Registration Failed, Please Try Again", Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
                });
    }

    private void writeNewUser(String userId, String userMail) {
        User user = new User(
                userMail,
                etPhoneRegister.getText().toString(),
                0
        );
        db.child(userId).setValue(user);

        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
        finish();
    }
}
