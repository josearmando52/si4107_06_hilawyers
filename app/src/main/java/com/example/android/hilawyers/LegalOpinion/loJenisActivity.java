package com.example.android.hilawyers.LegalOpinion;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;

import com.example.android.hilawyers.R;

public class loJenis extends AppCompatActivity {

    RadioGroup rbJenisLo, rbKasus1, rbKasus2;

    boolean isCheckedTipe = false;
    int checkIdTipe = -1;
    int checkIdJenis = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lo_jenis);
        ActionBar bar = getSupportActionBar();
        bar.setTitle("Legal Opinion");

        rbJenisLo = findViewById(R.id.rbJenisLo);
        rbKasus1 = findViewById(R.id.rbKasus1);
        rbKasus2 = findViewById(R.id.rbKasus2);

        if (savedInstanceState != null) {
            checkIdJenis = savedInstanceState.getInt("Jenis");
            checkIdTipe = savedInstanceState.getInt("Tipe");

            rbJenisLo.check(checkIdJenis);
            rbKasus1.check(checkIdTipe);
            rbKasus2.check(checkIdTipe);
        }

        rbJenisLo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                checkIdJenis = checkedId;
            }
        });

        rbKasus1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId != -1 && !isCheckedTipe) {
                    isCheckedTipe = true;
                    rbKasus2.clearCheck();
                    checkIdTipe = checkedId;
                }
                isCheckedTipe = false;
            }
        });

        rbKasus2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId != -1 && !isCheckedTipe) {
                    isCheckedTipe = true;
                    rbKasus1.clearCheck();
                    checkIdTipe = checkedId;
                }
                isCheckedTipe = false;
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("Jenis", checkIdJenis);
        outState.putInt("Tipe", checkIdTipe);
    }

    public void goToInformasiLO(View view) {
        startActivity(new Intent(loJenis.this, loInformasiActivity.class));
    }
}
