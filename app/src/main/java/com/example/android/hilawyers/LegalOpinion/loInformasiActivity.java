package com.example.android.hilawyers.LegalOpinion;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.android.hilawyers.R;

public class lo_data extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lo_informasi);
        ActionBar bar = getSupportActionBar();
        bar.setTitle("Legal Opinion");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void goToPilihPengacaraLO(View view) {
        startActivity(new Intent(lo_data.this, loPilihPengacaraActivity.class));
    }
}
