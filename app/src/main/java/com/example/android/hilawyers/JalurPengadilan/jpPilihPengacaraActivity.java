package com.example.android.hilawyers.JalurPengadilan;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.android.hilawyers.R;

public class jpPilihPengacaraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jp_pilih_pengacara);

        ActionBar bar = getSupportActionBar();
        bar.setTitle("Jalur Pengadilan");
    }

    public void goToCariPengacara(View view) {
        startActivity(new Intent(jpPilihPengacaraActivity.this, jpCariPengacaraActivity.class));
    }
}
