package com.example.android.hilawyers.PendampinganKepolisian;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.android.hilawyers.LegalOpinion.loSummaryActivity;
import com.example.android.hilawyers.R;
import com.example.android.hilawyers.Utility.SharedPrefs;

public class pkInformasiActivity extends AppCompatActivity {

    EditText etNamaInfo, etTTLInfo, etKTPInfo, etAlamatInfo;
    RadioGroup rgJenisKelamin;
    int checkId = -1;
    SharedPrefs sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pk_informasi);
        ActionBar bar = getSupportActionBar();
        bar.setTitle("Pendampingan di Kepolisian");

        sp = new SharedPrefs(this);

        etNamaInfo = findViewById(R.id.etNamaInfo);
        etTTLInfo = findViewById(R.id.etTTLInfo);
        etKTPInfo = findViewById(R.id.etKTPInfo);
        etAlamatInfo = findViewById(R.id.etAlamatInfo);
        rgJenisKelamin = findViewById(R.id.rgJenisKelamin);

        rgJenisKelamin.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                checkId = checkedId;
            }
        });
    }

    public void goToSummaryPK(View view) {
        if (TextUtils.isEmpty(etNamaInfo.getText())) {
            etNamaInfo.setError("Harap Diisi");
        } else if (TextUtils.isEmpty(etTTLInfo.getText())) {
            etNamaInfo.setError("Harap Diisi");
        } else if (TextUtils.isEmpty(etKTPInfo.getText())) {
            etKTPInfo.setError("Harap Diisi");
        } else if (TextUtils.isEmpty(etAlamatInfo.getText())) {
            etAlamatInfo.setError("Harap Diisi");
        } else if (checkId < 0) {
            Toast.makeText(getApplicationContext(), "Harap Pilih Jenis Kelamin",Toast.LENGTH_SHORT)
                    .show();
        } else {
            sp.setValue(SharedPrefs.NAMA,etNamaInfo.getText().toString());
            sp.setValue(SharedPrefs.TTL,etTTLInfo.getText().toString());
            sp.setValue(SharedPrefs.KTP,etKTPInfo.getText().toString());
            sp.setValue(SharedPrefs.ALAMAT,etAlamatInfo.getText().toString());

            RadioButton selected = findViewById(checkId);
            sp.setValue(SharedPrefs.JENIS_KELAMIN,selected.getText().toString());

            startActivity(new Intent(getApplicationContext(), pkSummaryActivity.class));
        }
    }
}
