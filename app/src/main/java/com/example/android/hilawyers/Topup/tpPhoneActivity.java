package com.example.android.hilawyers.Topup;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.example.android.hilawyers.R;

public class tpPhoneActivity extends AppCompatActivity {

    EditText etPhoneTopup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tp_phone);
        ActionBar bar = getSupportActionBar();
        bar.setTitle("Top Up");

        etPhoneTopup = findViewById(R.id.etPhoneTopup);
    }

    public void goToNominal(View view) {
        if (TextUtils.isEmpty(etPhoneTopup.getText())) {
            etPhoneTopup.setError("Harap Isi Nomor Telepon Anda");
            return;
        }
        startActivity(new Intent(tpPhoneActivity.this, tpNominalActivity.class));
    }
}
