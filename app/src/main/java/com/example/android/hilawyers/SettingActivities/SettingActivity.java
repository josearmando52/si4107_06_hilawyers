package com.example.android.hilawyers.SettingActivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.android.hilawyers.R;

public class SettingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
    }

    //Untuk berpindah ke activity ChangePasswordActivity
    public void goToChangePassword(View view) {
        startActivity(new Intent(SettingActivity.this, ChangePasswordActivity.class));
    }
}
