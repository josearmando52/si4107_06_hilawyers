package com.example.android.hilawyers.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;

import com.example.android.hilawyers.JalurPengadilan.jpKasusActivity;
import com.example.android.hilawyers.Model.User;
import com.example.android.hilawyers.R;
import com.example.android.hilawyers.Topup.tpPhoneActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.NumberFormat;
import java.util.Locale;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    DatabaseReference db;
    Button btnTopupHome;
    TextView tvBalanceHome;
    CardView cvJP, cvLO, cvMN, cvPk;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        db = FirebaseDatabase.getInstance().getReference();
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        btnTopupHome = root.findViewById(R.id.btnTopUpHome);
        btnTopupHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), tpPhoneActivity.class));
            }
        });

        cvJP = root.findViewById(R.id.cvJP);
        cvJP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), jpKasusActivity.class));
            }
        });

        tvBalanceHome = root.findViewById(R.id.tvBalanceHome);

        homeViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                tvBalanceHome.setText(s);
            }
        });

        return root;
    }

    private ValueEventListener listener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            User user = dataSnapshot.child("user").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).getValue(User.class);
            tvBalanceHome.setText(getActivity().getResources().getString(
                    R.string.rp,
                    NumberFormat.getNumberInstance(Locale.GERMANY).format(user.getBalance())
                    ));
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    @Override
    public void onStart() {
        super.onStart();
        db.addValueEventListener(listener);
    }

    @Override
    public void onStop() {
        super.onStop();
        db.removeEventListener(listener);
    }
}
