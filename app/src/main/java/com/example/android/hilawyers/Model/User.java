package com.example.android.hilawyers.Model;

public class User {
    private String email;
    private String phone;
    private int balance;

    public User() {
    }

    public User(String email, String phone, int balance) {
        this.email = email;
        this.phone = phone;
        this.balance = balance;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public int getBalance() {
        return balance;
    }
}
