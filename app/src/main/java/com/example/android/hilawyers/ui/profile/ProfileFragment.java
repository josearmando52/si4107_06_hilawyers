package com.example.android.hilawyers.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.arch.lifecycle.ViewModelProviders;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.hilawyers.LoginActivity;
import com.example.android.hilawyers.R;
import com.example.android.hilawyers.SettingActivities.SettingActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ProfileFragment extends Fragment {

    private ProfileViewModel profileViewModel;
    TextView tvEmailProfile;
    ImageView ivSettingProfile;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        profileViewModel =
                ViewModelProviders.of(this).get(ProfileViewModel.class);
        View root = inflater.inflate(R.layout.fragment_profile, container, false);
        tvEmailProfile = root.findViewById(R.id.tvEmailProfile);
        ivSettingProfile = root.findViewById(R.id.ivSettingProfile);
        final Button logout = root.findViewById(R.id.logout);

        tvEmailProfile.setText(FirebaseAuth.getInstance().getCurrentUser().getEmail());

        ivSettingProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SettingActivity.class));
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(getActivity(), LoginActivity.class));
                getActivity().finish();
            }
        });
        return root;
    }
}
