package com.example.android.hilawyers.JalurPengadilan;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toolbar;

import com.example.android.hilawyers.R;

public class jpKasusActivity extends AppCompatActivity {

    RadioGroup rbJenisKasus, rbTipeKasus1, rbTipeKasus2;

    boolean isCheckedTipe = false;
    int checkIdTipe = -1;
    int checkIdJenis = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jp_kasus);
        ActionBar bar = getSupportActionBar();
        bar.setTitle("Jalur Pengadilan");

        rbJenisKasus = findViewById(R.id.rbJenisKasus);
        rbTipeKasus1 = findViewById(R.id.rbTipeKasus1);
        rbTipeKasus2 = findViewById(R.id.rbTipeKasus2);

        if (savedInstanceState != null) {
            checkIdJenis = savedInstanceState.getInt("Jenis");
            checkIdTipe = savedInstanceState.getInt("Tipe");

            rbJenisKasus.check(checkIdJenis);
            rbTipeKasus1.check(checkIdTipe);
            rbTipeKasus2.check(checkIdTipe);
        }

        rbJenisKasus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                checkIdJenis = checkedId;
            }
        });

        rbTipeKasus1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId != -1 && !isCheckedTipe) {
                    isCheckedTipe = true;
                    rbTipeKasus2.clearCheck();
                    checkIdTipe = checkedId;
                }
                isCheckedTipe = false;
            }
        });

        rbTipeKasus2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId != -1 && !isCheckedTipe) {
                    isCheckedTipe = true;
                    rbTipeKasus1.clearCheck();
                    checkIdTipe = checkedId;
                }
                isCheckedTipe = false;
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("Jenis", checkIdJenis);
        outState.putInt("Tipe", checkIdTipe);
    }

    public void goToInformasi(View view) {
        startActivity(new Intent(jpKasusActivity.this,jpInformasiActivity.class));
    }
}
