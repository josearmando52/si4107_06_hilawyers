package com.example.android.hilawyers.SettingActivities;

import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.hilawyers.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ChangePasswordActivity extends AppCompatActivity {
    //et01dPassword merupakan variabel untuk password sebelumnya dan etNewPassword untuk password yang baru
    EditText etOldPassword, etNewPassword;
    FirebaseUser fUser;
    DatabaseReference db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        //button Ubah Password
        ActionBar bar = getSupportActionBar();
        bar.setTitle("Ubah Password");

        fUser = FirebaseAuth.getInstance().getCurrentUser();
        db = FirebaseDatabase.getInstance().getReference().child("user").child(fUser.getUid());

        etOldPassword = findViewById(R.id.etOldPassword);
        etNewPassword = findViewById(R.id.etNewPassword);
    }

    public void changePassword(View view) {
        //Sebagai Notifikasi jika kolom Password Lama dan baru tidak terisi maka akan menampilkan notifikasi Isi Password Lama/Baru
        if (TextUtils.isEmpty(etOldPassword.getText())) {
            etOldPassword.setError("Isi Password Lama");
            return;
        } else if (TextUtils.isEmpty(etNewPassword.getText())) {
            etNewPassword.setError("Isi Password Baru");
            return;

         //Memberikan Notifikasi jika Password lama dan Baru yang dimasukkan masih sama maka akan ditolak dan memberikan Notifikasi seperti dibawah
        } else if (TextUtils.equals(etOldPassword.getText(), etNewPassword.getText())) {
            Toast.makeText(ChangePasswordActivity.this, "Password Baru dan Lama Tidak Boleh Sama", Toast.LENGTH_SHORT).show();
            return;

         //Jika kondisi diatas sudah dipenuhi maka akan mengganti password lama menjadi yang baru
        } else {
            final String email = fUser.getEmail();
            AuthCredential credential = EmailAuthProvider.getCredential(email, etOldPassword.getText().toString());
            fUser.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {

                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        updatePassword(etNewPassword.getText().toString());

                        //jika password lama yang dimasukkan salah akan menampilkan notifikasi password lama salah
                    } else {
                        Toast.makeText(ChangePasswordActivity.this, "Password Lama Salah", Toast.LENGTH_SHORT).show();
                        etOldPassword.setError("Password Salah");
                    }
                }
            });
        }
    }

    private void updatePassword(String newPass) {
        fUser.updatePassword(newPass).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                //Setelah Password berhasil diubah nanti akan mengeluarkan Toast berupa Password Berhasil Diubah
                if (task.isSuccessful()) {
                    Toast.makeText(ChangePasswordActivity.this,"Password Berhasil Diubah", Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        });
    }
}
