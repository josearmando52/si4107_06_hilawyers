package com.example.android.hilawyers.JalurPengadilan;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.android.hilawyers.R;

public class jpInformasiActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jp_informasi);
        ActionBar bar = getSupportActionBar();
        bar.setTitle("Jalur Pengadilan");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void goToPilihPengacara(View view) {
        startActivity(new Intent(jpInformasiActivity.this, jpPilihPengacaraActivity.class));
    }
}
