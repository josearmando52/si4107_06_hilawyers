package com.example.android.hilawyers.PendampinganKepolisian;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.android.hilawyers.LegalOpinion.loSummaryActivity;
import com.example.android.hilawyers.Pengacara.PilihPengacaraActivity;
import com.example.android.hilawyers.R;
import com.example.android.hilawyers.Utility.SharedPrefs;

public class pkSummaryActivity extends AppCompatActivity {

    TextView tvKasusPK;
    SharedPrefs sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pk_summary);
        ActionBar bar = getSupportActionBar();
        bar.setTitle("Pendampingan di Kepolisian");

        sp = new SharedPrefs(this);

        tvKasusPK = findViewById(R.id.tvKasusPK);
        tvKasusPK.setText(sp.getValue(SharedPrefs.KASUS));
    }

    public void goToCariPengacaraPK(View view) {
        sp.setValue(SharedPrefs.KATEGORI,"Pendampingan di Kepolisian");
        startActivity(new Intent(pkSummaryActivity.this, PilihPengacaraActivity.class));
    }
}
